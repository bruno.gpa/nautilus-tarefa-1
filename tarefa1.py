
def task_1(n1, nx, r):
	'''
	Recebe três inteiros n1, nx e r e retorna a PA (se r for par) ou PG (se r for ímpar) de razão r
	com a1 = n1 e an <= nx'''

	output, n = [], 1

	if r % 2 == 0:
		while n1 + (n-1)*r <= nx:			# Enquanto PA <= nx, adiciona o último termo da PA ao output
			output.append(n1 + (n-1)*r)
			n += 1
	else:
		while n1 * r**(n-1) <= nx:			# Enquanto PG <= nx, adiciona o último termo da PG ao output
			output.append(n1 * r**(n-1))
			n += 1

	return output


def task_2(num):
	'''
	Recebe um inteiro num e retorna o número de bits iguais a 1 na forma binária deste inteiro'''

	binary = bin(num)[2:]			# Transforma o número em binário e retira o prefixo '0b'
	ones = binary.count('1')		# Conta o número de bits iguais a 1
	return ones


def task_3():
	'''
	Retorna o número de formas diferentes de obter duas libras com moedas de libra ou pence'''

	coins = [1, 2, 5, 10, 20, 50, 100, 200]			# Moedas disponíveis
	count = [1] + [0 for i in range(200)]			# Lista [1, 0, ... , 0] len = 201

	# Para cada moeda, conta de quantas formas é possível obter este valor com moedas menores ou iguais
	# Retorna a última casa da lista (moeda de 2 libras = 200 pence)
	for coin in coins:
		for i in range(coin, 201):
			count[i] += count[i-coin]

	return count[200]


def task_4():
	'''
	Retorna os dez últimos algarismos da sére 1^1 + 2^2 + ... + 1000^1000'''

	num = 0
	for x in range(1, 1001):
		num += pow(x, x, 10 ** 10)		# Calcula apenas os últimos 10 algarismos de cada potência
	return num % (10 ** 10)				# Retorna os últimos 10 algarismos do resultado


# Testes das funções
print(f'Função 1: {task_1(2, 13, 4)}')
print(f'Função 2: {task_2(1234)}')
print(f'Função 3: {task_3()}')
print(f'Função 4: {task_4()}')
