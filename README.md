# Nautilus tarefa de Python básico

Como executar o arquivo: as quatro funções do exercício estão no arquivo tarefa1.py. O arquivo também inclui testes para essas funções da forma como foram exemplificadas nos enunciados. Novos testes podem ser adicionados para garantir o funcionamento das funções. Não é necessária nenhuma biblioteca para executar este programa.
